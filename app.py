# Jennifer Páramo Mascote
# 14/07/2023
# Assessment Técnico - Backend Trainee
# Biblioteca 
# 

from flask import Flask, jsonify, request, render_template
from flask_mysqldb import MySQL
import hashlib
import jwt
import datetime

app = Flask(__name__)

# Página 
@app.route('/')
def index():
    register_response = request.args.get('register_response')
    login_response = request.args.get('login_response')
    create_book_response = request.args.get('create_book_response')
    return render_template('index.html', register_response=register_response, login_response=login_response, create_book_response=create_book_response)

# Base de datos MySQL
""" CREATE DATABASE biblioteca_at;
    USE biblioteca_at;
    CREATE TABLE IF NOT EXISTS users (   
        id INT(255) PRIMARY KEY AUTO_INCREMENT,
        email VARCHAR(255) NOT NULL UNIQUE,
        name VARCHAR(255),
        password VARCHAR(255)
    );

    CREATE TABLE IF NOT EXISTS books (
        id INT(255),
        author VARCHAR(255),
        isbn VARCHAR(255) NOT NULL UNIQUE,
        release_date DATE,
        title VARCHAR(500),
        user_id INT,
        users_id INT(255),
        FOREIGN KEY(users_id) REFERENCES users(id)
    );          
"""

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'biblioteca_at'

# Inicializar la extensión MySQL
mysql = MySQL(app)
# tokens
app.config['SECRET_KEY'] = 'Assessment_Técnico_Backend_Trainee'

# token expira en 24 hrs
time = datetime.datetime.utcnow()
expires = time + datetime.timedelta(hours=24)

# Verificación del token d
def verify_token(token):
    try:
        decoded = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
        return True
        # Token expirado
    except jwt.ExpiredSignatureError:
        return False
        # Token no válido
    except jwt.InvalidTokenError:
        return False

# verificación isbn
def isbn_exists(isbn):
    conexion = mysql.connection
    cursor = conexion.cursor()
    cursor.execute("SELECT id FROM books WHERE isbn = %s", (isbn,))
    result = cursor.fetchone()
    cursor.close()
    conexion.close()

    if result:
        return True
    else:
        return False


# 3 | 

# a) POST /auth/register:
@app.route('/auth/register', methods=['POST'])
def register():
    name = request.json.get('name')
    email = request.json.get('email')
    password = request.json.get('password')

    #encriptar contraseña con algoritmo MD5
    hasher = hashlib.md5()
    hasher.update(password.encode('utf-8'))
    hashed_password = hasher.hexdigest()

    if not name or not email or not password:
        return jsonify({'error': 'error'}), 400

    # almacenar en base de datos
    conexion = mysql.connection
    cursor = conexion.cursor()

    try:
        cursor.execute("INSERT INTO users (name, email, password) VALUES (%s, %s, %s)", (name, email, hashed_password))
        mysql.connection.commit()

        return jsonify({'message': 'Registro exitoso'})
    except Exception as e:
        cursor.close()
        return jsonify({'error': 'Error en registro'}), 500
    cursor.close()
    conexion.close()

# b. POST /auth/login:
@app.route('/auth/login', methods=['POST'])
def login():
    email = request.json.get('email')
    password = request.json.get('password')
    #encriptar password 
    hasher = hashlib.md5()
    hasher.update(password.encode('utf-8'))
    hashed_password = hasher.hexdigest()

    conexion = mysql.connection
    cursor = conexion.cursor()

    # verificar correo
    print("Email: ", email)
    print("Password: ", password)
    print("Password: ", hashed_password)
    cursor.execute("SELECT * FROM users WHERE email = %s", (email,))
    user = cursor.fetchone()
   
    if not user:
        return jsonify({'error': 'Email no encontrado'}), 404
    
    # verificar contraseña
    cursor.execute("SELECT * FROM users WHERE email = %s AND password = %s", (email, hashed_password))
    user = cursor.fetchone()

    cursor.close()
    # generar token
    if user:
        token = jwt.encode({'email': email, 'exp': expires}, app.config['SECRET_KEY'], algorithm='HS256')
        return jsonify({'token': token})
    # 
    else:
        return jsonify({'error': 'Contraseña incorrecta'}), 401
    
    
    conexion.close()
    

# c. GET /books:
@app.route('/books', methods=['GET'])
def get_books():
    conexion = mysql.connection
    cursor = conexion.cursor()
    cursor.execute("SELECT books.id, books.title, users.name AS user_name FROM books JOIN users ON books.users_id = users.id")
    books = cursor.fetchall()
    print("Libros:", books)
    cursor.close()
    return jsonify(books)
    conexion.close()


# d. POST /books:
@app.route('/books', methods=['POST'])
def create_books():
    isbn = request.json.get('isbn')
    title = request.json.get('title')
    author = request.json.get('author')
    release_date = request.json.get('release_date')

    print("Libro a insertar: ", isbn, title, author, release_date)
    conexion = mysql.connection
    cursor = conexion.cursor()
   #token = request.headers.get('Authorization')

# Verificar si el token de autenticación es válido
   #if not verify_token(token):
   #     return jsonify({'error': 'Token de autenticación inválido.'}), 401
   
    # ver que isbn no exista
    if isbn_exists(isbn):
        return jsonify({'error': 'isbn ya existente.'}), 409

    try:
        cursor.execute("INSERT INTO books (isbn, title, author, release_date) VALUES (%s, %s, %s, %s)", (isbn, title, author, release_date))
        
        mysql.connection.commit()
        book_id = cursor.lastrowid
        
        if book_id:
            #devolver el id del nuevo libro.
            return jsonify({'id': book_id}), 200
        else:
            return jsonify({'error': 'Error'}), 500
    except Exception as e:
        conexion.rollback()
        return jsonify({'error': 'Error'}), 500
    cursor.close()
    conexion.close()

# e. DELETE /books/:id:
@app.route('/books/<int:book_id>', methods=['DELETE'])
def delete_book(book_id):
    token = request.headers.get('Authorization')
    # Validar el token
    #if not verify_token(token):
        #return jsonify({'error': 'Token no valido'}), 401

    conexion = mysql.connection
    cursor = conexion.cursor()

    cursor.execute("SELECT * FROM books WHERE id = %s", (book_id,))
    book = cursor.fetchone()
    print("Libro a eliminar: ", book)
    if not book:
        cursor.close()
        return jsonify({'error': 'El libro no existe'}), 404
# Eliminar el libro de la base de datos
    try:
        cursor.execute("DELETE * FROM books WHERE id = %s", (book_id,))
        conexion.commit()
        cursor.close()
        return jsonify(True)
    
    except Exception as e:
        conexion.rollback()
        cursor.close()
        return jsonify({'error': 'Error'}), 500
    
    conexion.close()

if __name__ == '__main__':
    app.run()